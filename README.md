Implementação dos padrões:

1) Adapter: usei o padrão Adapter para realizar a persistência dos usuários e dos carros.

2) Factory Method: usei o padrão Factory para abstrair o método que utilizei para realizar a persistência dos carros nas classes de
                   controle.
				   