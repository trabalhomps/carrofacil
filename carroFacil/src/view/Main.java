package view;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        JFrame frameMain = new JFrame("Frame Principal");
        JFrame frameOptions = new JFrame("Frame das Opções");
        JFrame frameErrors = new JFrame("Frame de erros");
        int option;
        do{
            option = Integer.parseInt(JOptionPane.showInputDialog(frameMain,
                    "1 - Cadastro de Carros" +
                            "\n2 - Cadastro de Clientes" +
                            "\n3 - Mostra as informações de um cliente" +
                            "\n4 - Mostra as informações de um carro" +
                            "\n5 - Atualiza as informações de um cliente" +
                            "\n6 - Atualiza as informações de um carro" +
                            "\n7 - Remove um cliente" +
                            "\n8 - Remove um carro" +
                            "\n9 - Exibe todos os clientes" +
                            "\n10 - Exibe todos os carros" +
                            "\n0 - Sair","Carro Fácil - Compra e venda de automóveis",
                    JOptionPane.QUESTION_MESSAGE));
            switch(option){
                case 1:
                    MenuCadastroCarros.adicionaCarro(frameOptions,frameErrors);
                    break;

            }
        }while(option != 0);
    }
}
