package view;

import business.control.CarManager;
import business.model.Appearance;
import business.model.Mechanics;
import business.model.Style;
import business.model.Transmission;
import business.util.AppearanceInvalidException;
import business.util.MechanicInvalidException;
import business.util.StyleInvalidException;
import business.util.TransmissionIllegalException;
import business.util.CityIllegalException;
import business.util.ChassiIllegalException;

import javax.swing.*;

/**
 * Created by thyago on 02/11/15.
 */
public class MenuCadastroCarros {

    public static void adicionaCarro(JFrame frameOptions,JFrame frameErrors) {
        CarManager carManager = new CarManager();
        Appearance ap = null;
        Mechanics me = null;
        Style carStyle = null;
        Transmission trans = null;
        String city = null;
        String chassi = null;
        String model = JOptionPane.showInputDialog(frameOptions, "Digite o modelo do carro: ",
                JOptionPane.QUESTION_MESSAGE);
        String color = JOptionPane.showInputDialog(frameOptions,"Digite a cor do carro: ",
                JOptionPane.QUESTION_MESSAGE);
        int year = Integer.parseInt(JOptionPane.showInputDialog(frameOptions,
                "Digite o ano do carro",JOptionPane.QUESTION_MESSAGE));
        float price = Float.parseFloat(JOptionPane.showInputDialog(frameOptions,
                "Digite o preço do carro",JOptionPane.QUESTION_MESSAGE));
        try{
            ap = carManager.addAppearance(model,color,year,price);
        }catch(AppearanceInvalidException ex){
            JOptionPane.showMessageDialog(frameErrors,ex.getMessage(),"ERRO",
                    JOptionPane.ERROR_MESSAGE);
        }
        String maker = JOptionPane.showInputDialog(frameOptions,"Digite o fabricante do carro: ",
                JOptionPane.QUESTION_MESSAGE);
        String cylinder = JOptionPane.showInputDialog(frameOptions,"Digite a quantidade de" +
                " cilindros do veículo: ",JOptionPane.QUESTION_MESSAGE);
        char cylinderCount = cylinder.charAt(0);
        float kilometresAge = Float.parseFloat(JOptionPane.showInputDialog(frameOptions,
                "Digite a quantidade de kilômetros rodados",JOptionPane.QUESTION_MESSAGE));
        String fuel = JOptionPane.showInputDialog(frameOptions,"Digite a tipo de" +
                " combustível do veículo: ",JOptionPane.QUESTION_MESSAGE);
        try {
            me = carManager.addMechanics(maker,cylinderCount,kilometresAge,fuel);
        }catch(MechanicInvalidException ex){
            JOptionPane.showMessageDialog(frameErrors,ex.getMessage(),"ERRO",
                    JOptionPane.ERROR_MESSAGE);
        }
        String style = JOptionPane.showInputDialog(frameOptions,"Digite o estilo do carro: ",
                JOptionPane.QUESTION_MESSAGE);
        try{
            carStyle = carManager.addStyle(style);
        }catch(StyleInvalidException ex){
            JOptionPane.showMessageDialog(frameErrors,ex.getMessage(),"ERRO",
                    JOptionPane.ERROR_MESSAGE);
        }
        int numberDoors = Integer.parseInt(JOptionPane.showInputDialog(frameOptions,"Digite a quantidade " +
                    "de portas",JOptionPane.QUESTION_MESSAGE));
        try{
            String transmission = JOptionPane.showInputDialog(frameOptions,"Digite a transmissão do carro: ",
                    JOptionPane.QUESTION_MESSAGE);
            trans = carManager.addTransmission(transmission);
        }catch (TransmissionIllegalException ex){
            JOptionPane.showMessageDialog(frameErrors,ex.getMessage(),"ERRO",JOptionPane.ERROR_MESSAGE);
        }
        try{
            String chassi_entrada = JOptionPane.showInputDialog(frameOptions,"Digite o chassi do carro: ",
                    JOptionPane.QUESTION_MESSAGE);
            chassi = carManager.addChassi(chassi_entrada);
        }catch (ChassiIllegalException ex){
            JOptionPane.showMessageDialog(frameErrors,ex.getMessage(),"ERRO",JOptionPane.ERROR_MESSAGE);
        }
        try{
            String cidade = JOptionPane.showInputDialog(frameOptions,"Digite a Cidade do carro: ",
                    JOptionPane.QUESTION_MESSAGE);
            city = carManager.addCity (cidade);
        }catch (CityIllegalException ex){
            JOptionPane.showMessageDialog(frameErrors,ex.getMessage(),"ERRO",JOptionPane.ERROR_MESSAGE);
        }
 
        carManager.addCar(ap,me,carStyle,numberDoors,trans,chassi,city);
    }
}
