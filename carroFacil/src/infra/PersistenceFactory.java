package infra;

/**
 * Created by aluno on 03/11/15.
 */
public class PersistenceFactory {

    public static VehiclePersistenceAdapter getVehiclePersistance(String choosePersistence){
        if(choosePersistence.equalsIgnoreCase("carFile")){
            CarFile file = new CarFile();
            return file;
        }return null;
    }

    public static UserPersistenceAdapter getUserPersistance(String choosePersistence){
        if(choosePersistence.equalsIgnoreCase("userFile")){
            UserFile file = new UserFile();
            return file;
        }return null;
    }
}
