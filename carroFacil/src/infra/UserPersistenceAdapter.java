package infra;

import java.util.List;

/**
 * Created by aluno on 03/11/15.
 */
public interface UserPersistenceAdapter {
    void save(List listUser);
    List load() throws InfraException;
}
