package infra;

import java.util.Map;

/**
 * Created by aluno on 03/11/15.
 */
public interface VehiclePersistenceAdapter {
    void save(Map mapVehicles);
    Map load() throws InfraException;
}
