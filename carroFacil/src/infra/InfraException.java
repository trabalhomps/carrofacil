package infra;

/**
 * Created by thyago on 26/10/15.
 */
public class InfraException extends Exception {

    private static final long serialVersionUID = -4176717944228612029L;

    public InfraException(String message){
        super(message);
    }
}
