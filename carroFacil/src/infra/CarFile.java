package infra;

import business.model.Car;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.*;

/**
 * Created by thyago on 26/10/15.
 */
public class CarFile implements VehiclePersistenceAdapter {

    public static Logger logger = Logger.getLogger(CarFile.class.getName());

    public CarFile(){
        try{
            Handler hdConsole = new ConsoleHandler();
            Handler hdArquivo = new FileHandler("relatorioLogCarros.txt");

            hdConsole.setLevel(Level.OFF);
            hdArquivo.setLevel(Level.OFF);

            UserFile.logger.addHandler(hdConsole);
            UserFile.logger.addHandler(hdArquivo);

            UserFile.logger.setUseParentHandlers(false);
        }catch(IOException ex){
            logger.severe("Ocorreu um erro no arquivo durante a execução do programa");
        }
    }

    public void save(Map<String,Car> mapCars){
        File file = new File("cars.bin");
        try{
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(mapCars);
            out.close();
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public Map<String,Car> load() throws InfraException{
        Map<String,Car> mapCars = new HashMap<String,Car>();
        File file = new File("cars.bin");
        ObjectInputStream objInput = null;
        InputStream in = null;
        if(!file.exists()){
            save(mapCars);
        }
        try {
            in = new FileInputStream(file);
            objInput = new ObjectInputStream(in);
            mapCars = (Map<String,Car>) objInput.readObject();
            return mapCars;
        }catch(NullPointerException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistência, tente mais tarde");
        }catch(IOException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistência, tente mais tarde");
        }catch(ClassNotFoundException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistência,  tente mais tarde");
        }finally{
            try{
                objInput.close();
                in.close();
            }catch(IOException e){
                logger.severe(e.getMessage());
            }
        }
    }
}
