package business.model;

import java.io.Serializable;

/**
 * Created by thyago on 26/10/15.
 */
public enum Style implements Serializable {
    HATCHBACK, SEDAN, MINIVAN, PERUA, CONVERSIVEL, CUPE, PICAPE, SUV, UTILITARIO, BUGGY;

    @Override
    public String toString(){
        switch(this){
            case HATCHBACK:
                return "Hatchback";
            case SEDAN:
                return "Sedan";
            case MINIVAN:
                return "Minivan";
            case PERUA:
                return "Perua";
            case CONVERSIVEL:
                return "Conversível";
            case CUPE:
                return "Cupê";
            case PICAPE:
                return "Picape";
            case SUV:
                return "SUV";
            case UTILITARIO:
                return "Utilitário";
            case BUGGY:
                return "Buggy";
        }return null;
    }

    public static Style getValue(String value){
       if(value.equalsIgnoreCase(HATCHBACK.toString()))
           return HATCHBACK;
        else if(value.equalsIgnoreCase(SEDAN.toString()))
           return SEDAN;
        else if(value.equalsIgnoreCase(MINIVAN.toString()))
           return MINIVAN;
        else if(value.equalsIgnoreCase(PERUA.toString()))
           return PERUA;
        else if(value.equalsIgnoreCase(CONVERSIVEL.toString()))
           return CONVERSIVEL;
        else if(value.equalsIgnoreCase(CUPE.toString()))
           return CUPE;
        else if(value.equalsIgnoreCase(PICAPE.toString()))
           return PICAPE;
        else if(value.equalsIgnoreCase(SUV.toString()))
           return SUV;
        else if(value.equalsIgnoreCase(UTILITARIO.toString()))
           return UTILITARIO;
        else if(value.equalsIgnoreCase(BUGGY.toString()))
           return BUGGY;
        return null;
    }
}
