package business.model;

import java.io.Serializable;

/**
 * Created by thyago on 26/10/15.
 */
public enum Transmission implements Serializable {
    MANUAL,AUTOMATICO;

    public String toString(){
        switch(this){
            case MANUAL:
                return "Manual";
            case AUTOMATICO:
                return "Automático";
        }return null;
    }

    public static Transmission getValue(String value){
        if(value.equalsIgnoreCase(MANUAL.toString()))
            return MANUAL;
        else if(value.equalsIgnoreCase(AUTOMATICO.toString()))
            return AUTOMATICO;
        return null;
    }
}
