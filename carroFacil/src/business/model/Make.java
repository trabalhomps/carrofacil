package business.model;

import java.io.Serializable;

/**
 * Created by thyago on 26/10/15.
 */
public enum Make implements Serializable {
    CHEVROLET, FIAT, FORD, HONDA, HYUNDAI, MITSUBISHI, PEUGEOT, RENAULT, TOYOTA, VOLKSWAGEN,AUDI,VOLVO;

    @Override
    public String toString(){
        switch (this){
            case CHEVROLET:
                return "Chevrolet";
            case FIAT:
                return "Fiat";
            case FORD:
                return "Ford";
            case HONDA:
                return "Honda";
            case HYUNDAI:
                return "Hyundai";
            case MITSUBISHI:
                return "Mitsubishi";
            case PEUGEOT:
                return "Peugeot";
            case RENAULT:
                return "Renault";
            case TOYOTA:
                return "Toyota";
            case VOLKSWAGEN:
                return "Volkswagen";
            case AUDI:
                return "Audi";
            case VOLVO:
                return "Volvo";
        }return null;
    }

    public static Make getValue(String value){
        if(value.equalsIgnoreCase(CHEVROLET.toString()))
            return Make.CHEVROLET;
        else if(value.equalsIgnoreCase(FIAT.toString()))
            return Make.FIAT;
        else if(value.equalsIgnoreCase(FORD.toString()))
            return Make.FORD;
        else if(value.equalsIgnoreCase(HONDA.toString()))
            return Make.HONDA;
        else if(value.equalsIgnoreCase(HYUNDAI.toString()))
            return Make.HYUNDAI;
        else if(value.equalsIgnoreCase(MITSUBISHI.toString()))
            return Make.MITSUBISHI;
        else if(value.equalsIgnoreCase(PEUGEOT.toString()))
            return Make.PEUGEOT;
        else if(value.equalsIgnoreCase(RENAULT.toString()))
            return Make.RENAULT;
        else if(value.equalsIgnoreCase(TOYOTA.toString()))
            return Make.TOYOTA;
        else if(value.equalsIgnoreCase(VOLKSWAGEN.toString()))
            return Make.VOLKSWAGEN;
        else if(value.equalsIgnoreCase(AUDI.toString()))
            return Make.AUDI;
        else if(value.equalsIgnoreCase(VOLVO.toString()))
            return Make.VOLVO;
        return null;
    }
}