package business.model;

/**
 * Created by thyago on 04/11/15.
 */
public class UserClient extends User {
    private String endereco;
    private String cpf;
    private String rg;
    private String telefone;

    public UserClient(String firstname, String lastname, String email, String password, String city, String endereco, String cpf, String rg, String telefone){
        super(firstname,lastname,email,password, city);
        this.endereco = endereco;
        this.cpf = cpf;
        this.rg = rg;
        this.telefone = telefone;
    }

    public UserClient(User user, String endereco, String cpf, String rg, String telefone){
        super(user.getFirstName(),user.getLastName(),user.getEmail(),user.getPassword(), user.getCity());
        this.endereco = endereco;
        this.cpf = cpf;
        this.rg = rg;
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelefone() { return telefone; }

    public void setTelefone(String telefone) { this.telefone = telefone; }

    public String toString(){
        return super.toString() + "\nEndereco: " + this.endereco + "\nCPF: " + this.cpf + "\nRG: " + this.rg
        + "Telefone: " + this.telefone;
    }
}
