package business.model;

/**
 * Created by thyago on 26/10/15.
 */
public class Mechanics {
    private Make maker;
    private char cylinderCount;
    private float kilometresAge;
    private String fuel;

    public Mechanics() { }

    public Mechanics(Make maker, char cylinderCount, float kilometresAge, String fuel) {
        this.maker = maker;
        this.cylinderCount = cylinderCount;
        this.kilometresAge = kilometresAge;
        this.fuel = fuel;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public Make getMaker() {
        return maker;
    }

    public void setMaker(Make maker) {
        this.maker = maker;
    }

    public char getCylinderCount() {
        return cylinderCount;
    }

    public void setCylinderCount(char cylinderCount) {
        this.cylinderCount = cylinderCount;
    }

    public float getKilometresAge() {
        return kilometresAge;
    }

    public void setKilometresAge(float kilometresAge) {
        this.kilometresAge = kilometresAge;
    }

    @Override
    public String toString() {
        return "Mechanics{" +
                "Fabricante = " + maker +
                ", Número de cilindradas = " + cylinderCount +
                ", Kilômetros Rodados = " + kilometresAge +
                "Combustível = " + fuel  + "}";
    }
}