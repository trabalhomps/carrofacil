package business.model;

/**
 * Created by thyago on 26/10/15.
 */
public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String city;

    public User(){
        this.firstName = "Sylvester";
        this.lastName = "Stalone";
        this.email = "sylvester_syl@expendable.com";
        this.password = "rockyForever";
        this.city = "Philadelphia";
    }

    public User(String firstName, String lastName, String email, String passwd,String city) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = passwd;
        this.city = city;

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String senha) {
        this.password = senha;
    }

    public String toString(){
        return "Name: " + firstName + " " + lastName + "\nE-mail: " + email + "\nPassword: " + password + "\nCidade: " + city;
    }
}