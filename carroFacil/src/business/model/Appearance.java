package business.model;

/**
 * Created by thyago on 26/10/15.
 */
public class Appearance {
    private String model;
    private Color color;
    private int year;
    private float price;

    public static int yearMAX = 2017;

    public Appearance() {}

    public Appearance(String model, Color color, int year, float price) {
        this.model = model;
        this.color = color;
        this.year = year;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Appearance{" +
                "Modelo = " + model + '\'' +
                ", Cor = '" + color.toString() + '\'' +
                ", Ano = " + year +
                ", Preço = " + price +
                '}';
    }
}