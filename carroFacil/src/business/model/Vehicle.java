package business.model;

/**
 * Created by thyago on 26/10/15.
 */
public interface Vehicle {
    String getColor();
    int getYear();
    float kilometresAge();
    int getCylinderCount();
    int getYearRange();
    String getMaker();
    String getModel();
    float getPrice();
    String getChassi();
    String getFuel();
    String getCity();
}
