package business.model;

/**
 * Created by thyago on 27/10/15.
 */
public enum Color {
    BRANCO, VERMELHO, PRETO, CINZA, AZUL, PRATA, AMARELO, LARANJA, VERDE;

    @Override
    public String toString(){
        switch(this){
            case BRANCO:
                return "Branco";
            case VERMELHO:
                return "Vermelho";
            case PRETO:
                return "Preto";
            case CINZA:
                return "Cinza";
            case AZUL:
                return "Azul";
            case PRATA:
                return "Prata";
            case AMARELO:
                return "Amarelo";
            case LARANJA:
                return "Laranja";
            case VERDE:
                return "Verde";
        }return null;
    }

    public static Color getValue(String value){
        if(value.equalsIgnoreCase(BRANCO.toString()))
            return BRANCO;
        else if(value.equalsIgnoreCase(VERMELHO.toString()))
            return VERMELHO;
        else if(value.equalsIgnoreCase(PRETO.toString()))
            return PRETO;
        else if(value.equalsIgnoreCase(CINZA.toString()))
            return CINZA;
        else if(value.equalsIgnoreCase(AZUL.toString()))
            return AZUL;
        else if(value.equalsIgnoreCase(PRATA.toString()))
            return PRATA;
        else if(value.equalsIgnoreCase(AMARELO.toString()))
            return AMARELO;
        else if(value.equalsIgnoreCase(LARANJA.toString()))
            return LARANJA;
        else if(value.equalsIgnoreCase(VERDE.toString()))
            return VERDE;
        return null;
    }
}