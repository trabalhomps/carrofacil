package business.model;

import java.util.List;

/**
 * Created by thyago on 05/11/15.
 */
public class UserVendor extends UserClient {
    private List<String> vehiclesForSale;
    private List<Report> reportSales;

    public UserVendor(String firstname, String lastname, String email, String password, String city, String endereco, String cpf, String rg, String telefone) {
        super(firstname, lastname, email, password, city, endereco, cpf, rg, telefone);
    }

    public UserVendor(User user, String endereco, String cpf, String rg, String telefone) {
        super(user, endereco, cpf, rg, telefone);
    }


}
