package business.model;

/**
 * Created by thyago on 25/10/15.
 */
public class Car implements Vehicle {
    private Appearance carLook;
    private Mechanics carMechanics;
    private Style styleCar;
    private int numberDoors;
    private Transmission typeTransmission;
    private String chassi;
    private String city;

    public Car(Appearance carLook, Mechanics carMechanics, Style styleCar, int numberDoors, Transmission typeTransmission, String chassi, String city) {
        this.carLook = carLook;
        this.carMechanics = carMechanics;
        this.styleCar = styleCar;
        this.numberDoors = numberDoors;
        this.typeTransmission = typeTransmission;
        this.chassi = chassi;
        this.city = city;
    }

    public int getCylinderCount(){ return carMechanics.getCylinderCount(); }

    public int getYear(){ return carLook.getYear(); }

    public float kilometresAge(){ return carMechanics.getKilometresAge(); }

    public String getColor(){ return carLook.getColor().toString(); }

    public int getYearRange(){ return carLook.getYear(); }

    public String getMaker(){ return carMechanics.getMaker().toString(); }

    public String getModel(){ return carLook.getModel(); }

    public float getPrice(){ return carLook.getPrice(); }

    @Override
    public String getChassi() { return chassi; }

    public String getFuel() { return carMechanics.getFuel(); }

    public String getCity() { return this.city; }

    public void setCity(String city) { this.city = city; }

    public void setChassi(String chassi) { this.chassi = chassi; }

    public Transmission getTransmission(){ return typeTransmission; }

    public Style getStyleCar() { return styleCar; }

    public int getNumberDoors() { return numberDoors; }

    public void setCarLook(Appearance carLook) { this.carLook = carLook; }

    public void setCarMechanics(Mechanics carMechanics) { this.carMechanics = carMechanics; }

    public void setStyleCar(Style styleCar) { this.styleCar = styleCar; }

    public void setNumberDoors(int numberDoors) { this.numberDoors = numberDoors; }

    public void setTypeTransmission(Transmission typeTransmission) { this.typeTransmission = typeTransmission; }

    public String toString(){
        return carLook.toString() + carMechanics.toString() +
                "\nEstilo: " + styleCar.toString() + "\nNúmero de Portas: " + numberDoors +
                "\nTransmissão: " + typeTransmission.toString();
    }
}
