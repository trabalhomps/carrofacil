package business.model;

import java.util.List;
import java.util.Map;

/**
 * Created by thyago on 04/11/15.
 */
public abstract class TemplateSale {
    Map<String,Object> buyVehicles;

    public final void sale(){
        assignVehicleToClient();
        removeVehiclePurchase();
    }

    public abstract Vehicle searchVehicle();
    public abstract UserClient searchClient();
    public abstract void assignVehicleToClient();
    public abstract void removeVehiclePurchase();

    public void registerNewSale(){

    }

    public String printReportSale(){

    }

    public List getAllSale(){

    }

    public void addSaleToReport(){

    }
}
