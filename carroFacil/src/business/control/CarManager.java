package business.control;

import business.model.*;
import business.util.*;
import infra.CarFile;
import infra.InfraException;
import infra.PersistenceFactory;
import infra.VehiclePersistenceAdapter;

import java.util.*;
import java.util.logging.Level;

/**
 * Created by thyago on 26/10/15.
 */
public class CarManager {

    private Map<String,Car> cadastroCarros;
    private VehiclePersistenceAdapter carPersistence;

    public CarManager(){
        carPersistence = PersistenceFactory.getVehiclePersistance("carFile");
        try{
            cadastroCarros = carPersistence.load();
        }catch(InfraException ex){
            CarFile.logger.log(Level.FINE,ex.getMessage());
        }
    }

    public Appearance addAppearance(String model, String color, int year, float price) throws AppearanceInvalidException {
        CarValidador.validateColor(color);
        CarValidador.validateYear(year);
        CarValidador.validatePrice(price);

        Appearance ap = new Appearance(model,Color.getValue(color),year,price);
        return ap;
    }

    public Mechanics addMechanics(String maker, char cylinderCount, float kilometresAge, String fuel) throws MechanicInvalidException {
        CarValidador.validateMaker(maker);
        CarValidador.validateCylinderCount(cylinderCount);
        CarValidador.validateKilometresAge(kilometresAge);
        CarValidador.validateFuel(fuel);

        Mechanics mechanics = new Mechanics(Make.getValue(maker),cylinderCount,kilometresAge,fuel);
        return mechanics;
    }

    public Style addStyle(String style) throws StyleInvalidException{
        CarValidador.validateStyle(style);

        return Style.getValue(style);
    }

    public Transmission addTransmission(String transmission) throws TransmissionIllegalException {
        CarValidador.validateTransmission(transmission);

        return Transmission.getValue(transmission);
    }

    public String addChassi(String chassi) throws ChassiIllegalException {
        CarValidador.validateChassi(chassi);

        return chassi;
    }
    
    public String addCity (String city) throws CityIllegalException {
        CarValidador.validateCity(city);
        
        return city;
    }

    public int addNumberDoors(int numberDoors) throws IllegalArgumentException {
        CarValidador.validateNumberDoors(numberDoors);
        return numberDoors;
    }

    /**
     *
     * @param carLook
     * @param mechanics
     * @param style
     * @param numberDoors
     * @param typeTransmission
     * Descrição: Método que constrói um carro. Note que ele só deve ser usado em conjunto com os métodos
     *          acima.
     */
    public void addCar(Appearance carLook, Mechanics mechanics, Style style, int numberDoors, Transmission typeTransmission,String chassi, String city){
        Car car = new Car(carLook,mechanics,style,numberDoors,typeTransmission,chassi,city);
        cadastroCarros.put(chassi,car);
        carPersistence.save(cadastroCarros);
    }

    /**
     *
     * @param chassi
     * @param newModel
     * @param newAppearance
     * @param newMechanics
     * @param newStyle
     * @param newNumberDoors
     * @param newTransmission
     * @throws CarNotFoundException
     * Descrição: função que recebe o modelo do carro, busca ele no Map e troca seus atributos pelos
     *             recebidos em new*. Caso não encontre o carro em questão, a função lança um
     *             CarNotFoundException.
     */
    public void update(String chassi,String newModel, Appearance newAppearance, Mechanics newMechanics, Style newStyle, int newNumberDoors, Transmission newTransmission, String city) throws CarNotFoundException, InfraException {
        Set<String> setKeys = new HashSet<String>(cadastroCarros.keySet());
        boolean found = false;
        for(String key : setKeys){
            if(key.equalsIgnoreCase(chassi)){
                found = true;
                cadastroCarros.remove(key);
                Car newCar = new Car(newAppearance,newMechanics,newStyle,newNumberDoors,newTransmission,chassi,city);
                cadastroCarros.put(chassi,newCar);
                carPersistence.save(cadastroCarros);
            }
        }if(!found) throw new CarNotFoundException("Carro não foi encontrado, tente com outro modelo.\n");
        cadastroCarros = null;
        cadastroCarros = carPersistence.load(); //lança

    }

    public void remove(String chassi) throws CarNotFoundException, InfraException {
        boolean found = false;
        Set<String> setKeys = new HashSet<String>(cadastroCarros.keySet());
        for(String key : setKeys){
            if(key.equalsIgnoreCase(chassi)){
                found = true;
                cadastroCarros.remove(key);
            }
        }if(!found) throw new CarNotFoundException("Carro não foi encontrado, tente com outro modelo.\n");
        cadastroCarros = null;
        cadastroCarros = carPersistence.load(); //throws InfraException
    }

    public Map<Appearance,Car> getAllCars() throws InfraException {
        try{
            Map<Appearance,Car> mapCars = carPersistence.load();
            return mapCars;
        }catch(InfraException ex){
            //carPersistence.logger.log(Level.SEVERE,ex.getMessage());
            throw new InfraException("Erro de persistência, tente novamente mais tarde.\n");
        }
    }

    public String showOneCar(String chassi) throws CarNotFoundException, InfraException {
        cadastroCarros = carPersistence.load();
        Set<String> setKeys = new HashSet<String>(cadastroCarros.keySet());
        boolean found = false;
        for(String key : setKeys){
            if(key.equalsIgnoreCase(chassi)) {
                found = true;
                return cadastroCarros.get(key).toString();
            }
        }if(!found){
            throw new CarNotFoundException("Carro não foi encontrado, tente com outro modelo.\n");
        }return null;
    }
}