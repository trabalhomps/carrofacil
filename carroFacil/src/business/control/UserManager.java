package business.control;

import business.model.User;
import business.util.*;
import infra.InfraException;
import infra.PersistenceFactory;
import infra.UserFile;
import infra.UserPersistenceAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by thyago on 26/10/15.
 */
public class UserManager {

    private List<User> users;
    private UserPersistenceAdapter userPersistence;

    public UserManager(){
        userPersistence = PersistenceFactory.getUserPersistance("userFile");
        try{
            users = userPersistence.load();
        }catch(InfraException e){
            UserFile.logger.log(Level.FINE,e.getMessage());
        }
    }

    public void add(String firstName,String lastName,String email, String passwd, String city) throws PasswordInvalidException, EmailInvalidException, NameInvalidException, CityIllegalException {
        UserValidador.validateName(firstName);
        UserValidador.validateName(lastName);
        UserValidador.validateEmail(email);
        UserValidador.validatePassword(passwd);
        UserValidador.validateCity(city);
        users.add(new User(firstName,lastName,email,passwd, city));
        userPersistence.save(users);
    }

    public void update(String email, String newFirstName, String newLastName, String newPasswd, String newCity) throws UserNotFoundException, InfraException {
        User newUser = new User(newFirstName,newLastName,email,newPasswd,newCity);
        boolean found = false;
        for(User us : users){
            if(us.getEmail().equals(email)){
                found = true;
                users.remove(us);
                users.add(newUser);
                userPersistence.save(users);
            }if(!found)
                throw new UserNotFoundException("O usuário a ser editado não pôde ser encontrado\n");
            users = null;
            users = userPersistence.load();
        }
    }

    public boolean remove(User u) throws UserNotFoundException, InfraException{
        boolean found = false;
        for(User us : users){
            if(us.getFirstName().equals(u.getFirstName()) && us.getLastName().equals(u.getLastName()) && us.getPassword().equals(u.getPassword())){
                found = true;
                users.remove(us);
                userPersistence.save(users);
            }
        }if(!found){
            throw new UserNotFoundException("O usuário a ser removido não pôde ser encontrado\n");
        }
        users = null;
        users = userPersistence.load();
        return true;
    }

    public List<User> getAllClients() throws InfraException {
        try{
            List<User> listClients = userPersistence.load();
            return listClients;
        }catch(NullPointerException ex){
            //UserFile.logger.severe(ex.getMessage());
            throw new InfraException("Erro de persistência, tente mais tarde");
        }
    }

    public String showOneClient(String email) throws UserNotFoundException {
        boolean found = false;
        List<User> listUsers = new ArrayList<User>(users);
        for(User u : listUsers){
            if(u.getEmail().equals(email)){
                found = true;
                return u.toString();
            }
        }if(!found){
            throw new UserNotFoundException("Usuário não encontrado, tente novamente usando outro email.\n");
        }return null;
    }

    public String showOneClient(String firstName,String lastName) throws UserNotFoundException {
        boolean found = false;
        List<User> listUsers = new ArrayList<User>(users);
        for(User u : listUsers){
            if(u.getFirstName().equalsIgnoreCase(firstName) && u.getLastName().equalsIgnoreCase(lastName)){
                found = true;
                return u.toString();
            }
        }if(!found){
            throw new UserNotFoundException("Usuário não encontrado, tente novamente com um novo nome.\n");
        }return null;
    }
}