package business.util;

/**
 * Created by thyago on 04/11/15.
 */
public class ChassiIllegalException extends Exception {

    private static final long serialVersionUID = 1L;

    public ChassiIllegalException(){
        super("Chassi Inválido. Tente novamente.\n");
    }

    public ChassiIllegalException(String message){
        super(message);
    }
}
