package business.util;

/**
 * Created by thyago on 01/11/15.
 */
public class CarNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public CarNotFoundException(){
        super("Carro não encontrado no sistema.\n");
    }

    public CarNotFoundException(String message){
        super(message);
    }
}
