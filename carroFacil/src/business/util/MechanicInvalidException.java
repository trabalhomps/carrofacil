package business.util;

/**
 * Created by thyago on 30/10/15.
 */
public class MechanicInvalidException extends Exception {

    private static final long serialVersionUID = 1L;

    public MechanicInvalidException(){
        super("Erro nas configurações da mecânica do veículo\n ");
    }

    public MechanicInvalidException(String message){
        super(message);
    }
}
