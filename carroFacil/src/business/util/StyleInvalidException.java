package business.util;

/**
 * Created by thyago on 31/10/15.
 */
public class StyleInvalidException extends Exception {

    private static final long serialVersionUID = 1L;

    public StyleInvalidException(){
        super("Estilo de carro inválido.\n");
    }

    public StyleInvalidException(String message){
        super(message);
    }
}
