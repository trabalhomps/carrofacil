package business.util;

/**
 * Created by thyago on 30/10/15.
 */
public class AppearanceInvalidException extends Exception{

    private static final long serialVersionUID = 1L;

    public AppearanceInvalidException(){
        super("Erro nas configurações de aparência do veículo.\n");
    }

    public AppearanceInvalidException(String message){
        super(message);
    }
}
