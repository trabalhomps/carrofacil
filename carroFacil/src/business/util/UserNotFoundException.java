package business.util;

/**
 * Created by thyago on 26/10/15.
 */
public class UserNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public UserNotFoundException(){
        super("Usuário não encontrado!\n");
    }

    public UserNotFoundException(String message){
        super(message);
    }
}
