package business.util;

import business.model.*;


/**
 * Created by thyago on 30/10/15.
 */
public class CarValidador {

    public static void validateColor(String color) throws AppearanceInvalidException {
        if(Color.getValue(color) == null)
            throw new AppearanceInvalidException("Cor não registrada no sistema.\n");
    }

    public static void validateYear(int year) throws AppearanceInvalidException {
        if(year < 0)
            throw new AppearanceInvalidException("Insira um valor não negativo para o ano.\n");
        if(year < 1970)
            throw new AppearanceInvalidException("Nós não registramos um veículo de ano inferior a 1970.\n");
        if(year > Appearance.yearMAX)
            throw new AppearanceInvalidException("Registre um valor válido para o ano.\n");
    }

    public static void validatePrice(float price) throws AppearanceInvalidException {
        if(price < 0)
            throw new AppearanceInvalidException("Insira um valor não negativo para o preço");
    }

    public static void validateMaker(String maker) throws MechanicInvalidException {
        if(Make.getValue(maker) == null)
            throw new MechanicInvalidException("Fabricante não registrado no sistema.\n");
    }

    public static void validateCylinderCount(char cylinder) throws MechanicInvalidException {
        if(cylinder < 0) throw new MechanicInvalidException("Insira um valor não negativo para a quantidade de cilindros.\n");
        if(Character.isLetter(cylinder)) throw new MechanicInvalidException("Insira um número para a quantidade de cilindros.\n");
    }

    public static void validateKilometresAge(float kilometres) throws MechanicInvalidException {
        if(kilometres < 0) throw new MechanicInvalidException("Insira um valor não negativo para a quantidade de kilômetros rodados.\n ");
    }
    
        public static void validateFuel(String fuel) throws MechanicInvalidException {
            if(fuel.trim().isEmpty() || fuel == null)
                throw new MechanicInvalidException ("Combustível não pode ser vazia.\n");
    }

    public static void validateStyle(String style) throws StyleInvalidException {
        if(Style.getValue(style) == null)
            throw new StyleInvalidException("Estilo de veículo não cadastrado no sistema.\n");
    }
    


    public static void validateNumberDoors(int numberDoors) throws IllegalArgumentException {
        if(numberDoors < 0 || numberDoors > 5)
            throw new IllegalArgumentException("Insira um valor válido para a quantidade de portas.\n");
    }

    public static void validateTransmission(String transmission) throws TransmissionIllegalException {
        if(Transmission.getValue(transmission) == null)
            throw new TransmissionIllegalException("Transmissão não reconhecida.\n");
    }

    public static void validateChassi(String chassi) throws ChassiIllegalException {
        //Regras para validação de chassi
    }
    
    public static void validateCity(String city) throws CityIllegalException {
        if(city.length() > 30)
            throw new CityIllegalException("Nome com mais de 30 caracteres!\n");
        else if( city == null || city.trim().isEmpty())
            throw new CityIllegalException("Campo vazio!\n");
        else if(city.matches(".*\\d.*"))
            throw new CityIllegalException("Campo cidade não pode possuir números!\n");
    }
}