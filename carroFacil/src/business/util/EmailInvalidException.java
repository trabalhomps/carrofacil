package business.util;

/**
 * Created by thyago on 26/10/15.
 */
public class EmailInvalidException extends Exception {

    private static final long serialVersionUID = 1L;

    public EmailInvalidException(){
        super("Email Inválido");
    }

    public EmailInvalidException(String message){
        super(message);
    }
}
