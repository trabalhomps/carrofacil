package business.util;

/**
 * Created by thyago on 26/10/15.
 */
public class UserValidador {

    public static void validateEmail(String email) throws EmailInvalidException{
        if(email.indexOf("@") == -1) throw new EmailInvalidException("Email Inválido!\n");
    }

    public static void validateName(String name) throws NameInvalidException {
        if(name.length() > 20)
            throw new NameInvalidException("Nome com mais de 20 caracteres!\n");
        else if(name == null || name.trim().isEmpty())
            throw new NameInvalidException("Campo vazio!\n");
        else if(name.matches(".*\\d.*"))
            throw new NameInvalidException("Campo nome não pode possuir números!\n");
    }

    public static void validatePassword(String pass) throws PasswordInvalidException   {

        if(!pass.matches(".*\\d.*") || !pass.matches(".*\\c.*"))
            throw new PasswordInvalidException("Senha deve possuir caracteres e números!\n");
        else if (countNumbers(pass) < 2)
            throw new PasswordInvalidException("Senha deve ter pelo menos 2 números!\n");
    }

    private static int countNumbers(String s){
        int count = 0;
        for (int i = 0, len = s.length(); i < len; i++) {
            if (Character.isDigit(s.charAt(i))) {
                count++;
            }
        }
        return count;
    }
    
    public static void validateCity(String city) throws CityIllegalException {
        if(city.length() > 30)
            throw new CityIllegalException("Nome com mais de 30 caracteres!\n");
        else if( city == null || city.trim().isEmpty())
            throw new CityIllegalException("Campo vazio!\n");
        else if(city.matches(".*\\d.*"))
            throw new CityIllegalException("Campo cidade não pode possuir números!\n");
    }
}
