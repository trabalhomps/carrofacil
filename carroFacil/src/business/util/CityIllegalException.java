/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.util;

/**
 *
 * @author Neto
 */
public class CityIllegalException extends Exception {
    
    private static final long serialVersionUID = 1L;

    public CityIllegalException(){
        super("Cidade não pode ser vazia.\n");
    }

    public CityIllegalException(String message){
        super(message);
    }
    
}
