package business.util;

/**
 * Created by thyago on 31/10/15.
 */
public class TransmissionIllegalException extends Exception {

    private static final long serialVersionUID = 1L;

    public TransmissionIllegalException(){
        super("Transmissão inválida.\n");
    }

    public TransmissionIllegalException(String message){
        super(message);
    }
}
