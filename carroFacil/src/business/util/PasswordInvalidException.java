package business.util;

/**
 * Created by thyago on 26/10/15.
 */
public class PasswordInvalidException extends Exception {

    private static final long serialVersionUID = 1L;

    public PasswordInvalidException(){
        super("Senha Inválida");
    }

    public PasswordInvalidException(String message){
        super(message);
    }
}
