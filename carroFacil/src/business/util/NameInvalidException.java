package business.util;

/**
 * Created by thyago on 26/10/15.
 */
public class NameInvalidException extends Exception {

    private static final long serialVersionUID = 1L;

    public NameInvalidException(){
        super("Nome Inválido");
    }

    public NameInvalidException(String message){
        super(message);
    }
}
